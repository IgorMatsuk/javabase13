package JavaBase13Jul.HW__1.functions;

/*
* 2. Вводим число (0-999), получаем строку с прописью числа.
* */
import java.util.Scanner;

public class WriteNumToString {
    static final String[] below_twenty = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одинадцать", "двенадцадь", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
    static final String[] tens = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    static final String[] hundrets = {"сто", "двести", "триста", "четыриста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Число от 0 до 999 ");
            if (!sc.hasNextInt())
                break;
            int num = sc.nextInt();
            if (num < 0 || num > 999) {
                System.out.println("Число вне диапазона от 0 до 999");
                continue;
            }
            if (num < 20)
                System.out.println(below_twenty[num]);
            else if (num < 100) {
                int high = num / 10;
                int low = num % 10;
                String text = tens[high - 1];
                if (low != 0)
                    text = text + " " + below_twenty[low];
                System.out.println(text);
            } else if (num < 1000) {
                int firstDig = num / 100;
                int secondDig = (num % 100) / 10;
                int thirdDig = num % 10;
                String text = hundrets[firstDig - 1];
                if (firstDig > 0 && secondDig > 0 && thirdDig > 0) {
                    text = text + " " + tens[secondDig-1] + " " + below_twenty[thirdDig];
                    System.out.println(text);
                } else if (firstDig > 0 && secondDig > 0 && thirdDig == 0 ){
                    text = text +" "+ tens[secondDig-1];
                    System.out.println(text);
                } else if (firstDig > 0 && secondDig == 0 && thirdDig > 0 ){
                    text = text +" "+ below_twenty[thirdDig];
                    System.out.println(text);
                } else System.out.println(text);
            }

        }

        sc.close();
    }
}
