package JavaBase13Jul.HW__1.condStat;
/*
2.Определить какой четверти принадлежит точка с координатами (х,у)
*/
public class condStat_2 {

    public static void main(String[] args) {
        int x = 12;
        int y = -5;

        if (x > 0 && y > 0)
            System.out.println("I");
        if (x < 0 && y > 0)
            System.out.println("II");
        if (x < 0 && y < 0)
            System.out.println("III");
        if (x > 0 && y < 0)
            System.out.println("IV");
    }

}
