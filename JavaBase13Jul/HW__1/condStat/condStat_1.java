package JavaBase13Jul.HW__1.condStat;
/*
1. Если а – четное посчитать а*б, иначе а+б
*/

public class condStat_1 {

    public static void main(String[] args) {
        int a = 18;
        int b = 7;

        if (a % 2 != 0)
            System.out.println(a + b);
        else
            System.out.println(a * b);
    }
}