package JavaBase13Jul.HW__1.condStat;
/*
5. Написать программу определения оценки студента по его рейтингу, на основе следующих правил
Рейтинг  Оценка
0-19   --- F
20-39  --- E
40-59  --- D
60-74  --- C
75-89  --- B
90-100 --- A
*/
public class condStat_5 {
    public static void main(String[] args) {
        int a = 20;

        if (a >= 0 && a < 20)
            System.out.println("F");
        if (a >= 20 && a < 40)
            System.out.println("E");
        if (a >= 40 && a < 60)
            System.out.println("D");
        if (a >= 60 && a < 75)
            System.out.println("C");
        if (a >= 75 && a < 90)
            System.out.println("B");
        if (a >= 90 && a < 100)
            System.out.println("A");
    }
}
