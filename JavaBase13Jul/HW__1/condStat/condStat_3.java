package JavaBase13Jul.HW__1.condStat;

/*
3.Найти суммы только положительных из трех чисел
*/
public class condStat_3 {
    public static void main(String[] args) {
        int a = -3, b = 5, c = -8;

        if (a > 0 && b > 0 && c > 0)
            System.out.println(a + b + c);
        if (a > 0 && b > 0 && c < 0)
            System.out.println(a + b);
        if (a > 0 && b < 0 && c > 0)
            System.out.println(a + c);
        if (a < 0 && b > 0 && c > 0)
            System.out.println(b + c);
    }

}
