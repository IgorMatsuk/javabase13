package JavaBase13Jul.HW__1.condStat;

/*
Посчитать выражение макс(а*б*с, а+б+с)+3
*/
public class condStat_4 {
    public static void main(String[] args) {
        int a, b, c;
        a = 5;
        b = 0;
        c = 7;

        if (((a * b * c) + 3) > ((a + b + c) + 3))
            System.out.println("max (a*b*c)+3= "+((a * b * c) + 3));
         else
            System.out.println("max (a+b+c)+3= " + ((a + b + c) + 3));

    }
}
