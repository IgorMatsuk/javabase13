package JavaBase13Jul.HW__1.arrays;

/*
 * 9. Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
 * */
public class SortArray_9 {
    public static void main(String[] args) {

    }

    public static void bubbleSort(int[] myArray) {
        for (int i = myArray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (myArray[j] > myArray[j + 1]) {
                    int tmp = myArray[j];
                    myArray[j] = myArray[j + 1];
                    myArray[j + 1] = tmp;
                }

            }

        }

    }

    public static void selectSort(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            int min = myArray[i];
            int min_i = i;
            for (int j = i + 1; j < myArray.length; j++) {
                if (myArray[j] < min) {
                    min = myArray[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = myArray[i];
                myArray[i] = myArray[min_i];
                myArray[min_i] = tmp;
            }
        }

    }

    public static void insertionSort(int[] myArray) {

        for (int left = 0; left < myArray.length; left++) {
            //Вытаскиваем значение элемента
            int value = myArray[left];
            //Перемещаемся по єлементам которые перед вытащеным элементом
            int i = left - 1;
            for (; i >= 0; i--) {
                //если вытащили значение меньшее - передвигаем больший элемент дальше
                if (value < myArray[i]) {
                    myArray[i + 1] = myArray[i];
                } else {
                    //если вытащенный елемент больше - останавливаемся
                    break;
                }
            }
            //в освободившееся место вставляем вытащенное значение
            myArray[i + 1] = value;
        }
    }

}
