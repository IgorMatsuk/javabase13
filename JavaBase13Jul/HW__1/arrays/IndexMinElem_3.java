package JavaBase13Jul.HW__1.arrays;

/*
 * 3. Найти индекс минимального элемента массива
 * */
public class IndexMinElem_3 {
    public static void main(String[] args) {
        int[] myArray = {256, 157, 3445, 76, 88, 22, 78, 87, 954, 5};
        int minElem = myArray[0];
        int minIdex = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (minElem > myArray[i]) {
                minElem = myArray[i];
                minIdex = i+1;
            }
        }
        //System.out.println("Длина масива " + myArray.length);
        //System.out.println("Минимальный элемент = " + minElem);
        System.out.println("Индекс минимального элемента = "+ minIdex);

    }

}
