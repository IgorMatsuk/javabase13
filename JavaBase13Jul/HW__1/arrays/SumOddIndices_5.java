package JavaBase13Jul.HW__1.arrays;
/*
* 5. Посчитать сумму элементов массива с нечетными индексами
* */
public class SumOddIndices_5 {
    public static void main(String[] args) {
        int[] myArray = {12, 45, 76, 34, 64, 22, 12, 34, 23, 99};
        int sumOddInd = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (i % 2 != 0) {
                sumOddInd = myArray[i]+sumOddInd;

            }
        }
        System.out.println("Сумма элементов массива с нечетными индексами = "+sumOddInd);

    }
}
