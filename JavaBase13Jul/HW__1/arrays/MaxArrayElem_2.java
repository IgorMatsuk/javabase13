package JavaBase13Jul.HW__1.arrays;

/*
 * 2. Найти максимальный элемент массива
 * */
public class MaxArrayElem_2 {
    public static void main(String[] args) {
        int[] myArray = {12, 45, 76, 34, 64, 22, 12, 34, 23, 99};
        int maxElem = myArray[0];

        for (int i = 0; i < myArray.length; i++) {
            if (maxElem < myArray[i]) {
                maxElem = myArray[i];
            }
        }
        //System.out.println("Длина масива " + myArray.length);
        System.out.println("Максимальный элемент = " + maxElem);
    }
}
