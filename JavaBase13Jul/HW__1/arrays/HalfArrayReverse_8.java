package JavaBase13Jul.HW__1.arrays;

/*
 * 8. Поменять местами первую и вторую половину массива, например,
 * для массива 1 2 3  4, результат 3 4 1 2
 * */
public class HalfArrayReverse_8 {
    public static void main(String[] args) {
        int[] myArray = {12, 45, 76, 34, 64, 22, 12, 34, 23, 99};
        int temp = 0;
        for (int i = 0; i < myArray.length / 2; i++) {
            temp = myArray[myArray.length - 1 - i];
            myArray[myArray.length - 1 - i] = myArray.length;
            myArray[i] = temp;
        }
        for (int i = 0; i < myArray.length; i++) {
            System.out.println(myArray[i]);

        }
    }
}

