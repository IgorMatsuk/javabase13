package JavaBase13Jul.HW__1.arrays;

/*
 * 6. Сделать реверс массива (массив в обратном направлении)
 *
 * */
public class ArrayReverse_6 {
    public static void main(String[] args) {
        int[] myArray = {12, 45, 76, 34, 64, 22, 12, 34, 23, 99};
        int temp = 0;
        for (int i = myArray.length - 1; i >= 0; i--) {
            System.out.print(myArray[i] + " ");
        }
    }
}
