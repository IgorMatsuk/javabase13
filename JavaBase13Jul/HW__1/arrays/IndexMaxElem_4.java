package JavaBase13Jul.HW__1.arrays;
/*
* 4. Найти индекс максимального элемента массива
* */
public class IndexMaxElem_4 {
    public static void main(String[] args) {
        int[] myArray = {12, 45, 76, 34, 64, 22, 12, 34, 100, 99};
        int maxElem = myArray[0];
        int maxIndex = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (maxElem < myArray[i]) {
                maxElem = myArray[i];
                maxIndex = i + 1;
            }
        }
        //System.out.println("Длина масива " + myArray.length);
        //System.out.println("Максимальный элемент = " + maxElem);
        System.out.println("Индекс максимального элемента = " + maxIndex);
    }
}

