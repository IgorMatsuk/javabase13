package JavaBase13Jul.HW__1.arrays;

/*
 * 1. Найти минимальный элемент массива
 * */
public class MinArrayElem_1 {
    public static void main(String[] args) {
        int[] myArray = {256, 157, 3445, 76, 88, 22, 78, 87, 954, 5};
        int minElem = myArray[0];
        for (int i = 0; i < myArray.length; i++) {
            if (minElem > myArray[i]) {
                minElem = myArray[i];
            }
        }
        //System.out.println("Длина масива "+myArray.length);
        System.out.println("Минимальный элемент = " + minElem);
    }
}