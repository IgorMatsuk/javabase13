package JavaBase13Jul.HW__1.cycles;

/*
 * 5. Посчитать сумму цифр заданного числа
 * */
public class NumberSum {
    public static void main(String[] args) {
        int result = 0;
        int num = 10;
        for (int i = 1; i <= num; i++) {
            result += i;
        }
        System.out.println("Сумма цифр числа " + num + " равна " + result);
    }

}