package JavaBase13Jul.HW__1.cycles;

/*
 * 4. Вычислить факториал числа n. n! = 1*2*…*n-1*n;!
 */
public class FactorialOfNumber {
    public static void main(String[] args) {
        int result = 1;
        int fact = 3;
        for (int i = 1; i <= fact; i++) {
            result *= i;
        }
        System.out.println(result);
    }
}
