package JavaBase13Jul.HW__1.cycles;

/*
2. Проверить простое ли число?
   (число называется простым, если оно делится только само на себя и на 1) 
*/
public class PrimeNumber {
    public static void main(String[] args) {
        int a = 29;
        for (int i = 2; i < a; i++) {
            if (a % i == 0) {
                System.out.println("Число " + a + " НЕ простое");
                return;
            }
        }
        System.out.println("Число " + a + " простое");
    }
}

