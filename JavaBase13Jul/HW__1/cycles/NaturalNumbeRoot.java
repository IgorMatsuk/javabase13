package JavaBase13Jul.HW__1.cycles;

/*
3.Найти корень натурального числа с точностью до целого
(рассмотреть вариант последовательного подбора и метод бинарного поиска)
*/
public class NaturalNumbeRoot {
    public static void main(String[] args) {
        int number = 56;
        int natSqrt = (int)Math.round(Math.sqrt(number));
        System.out.println("Корень натурального числа "+number+" с точностью до целого равен "+natSqrt);
    }
}
