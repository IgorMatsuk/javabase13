package JavaBase13Jul.HW__1.cycles;

/*
1. Найти сумму четных чисел и их количество в диапазоне от 1 до 99
*/
public class SumEveNumbers {
    public static void main(String[] args) {
        int sum = 0;
        int number = 0;

        for (int i = 1; i <= 99; i++) {
            if (i % 2 == 0) {
                number++;
                sum = sum + i;
            }
        }
        System.out.println("Количество четных чисел = " + number);
        System.out.println("Сумма четных чисел = " + sum);
    }
}
