package JavaBase13Jul.HW__1.cycles;

/*
6. Вывести число, которое является зеркальным отображением последовательности цифр
 заданного числа, например, задано число 123, вывести 321.
 */
public class NumbersMirror {
    public static void main(String[] args) {
        int n = 123;
        while (n > 0) {
            System.out.print(n % 10);
            n /= 10;
        }
    }
}
